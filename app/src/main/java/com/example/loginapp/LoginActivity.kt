package com.example.loginapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {


    private lateinit var ETEmail: EditText
    private lateinit var ETPassword: EditText
    private lateinit var ETRepeatPassword: EditText
    private lateinit var buttonSubmit: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
        submitListener()



    }

    private fun init(){
        ETEmail = findViewById(R.id.ETEmail)
        ETPassword = findViewById(R.id.ETPassword)
        ETRepeatPassword = findViewById(R.id.ETRepeatPassword)
        buttonSubmit = findViewById(R.id.buttonSubmit)

    }


    private fun submitListener(){
        buttonSubmit.setOnClickListener {
            val email = ETEmail.text.toString()

            val password = ETPassword.text.toString()

            val repeatPassword = ETRepeatPassword.text.toString()

            if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(this, "E-mail Invalid", Toast.LENGTH_SHORT).show()
                return@setOnClickListener


            }

            if (password.length < 9 || password != repeatPassword){
                Toast.makeText(this, "password is Invalid", Toast.LENGTH_SHORT).show()
                return@setOnClickListener


            }



            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password).addOnCompleteListener { task ->
                if (task.isSuccessful){
                    Toast.makeText(this, "Great Success", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "something went wrong", Toast.LENGTH_SHORT).show()
                }
            }



        }


    }

}